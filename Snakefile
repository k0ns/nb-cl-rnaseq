import pandas as pd

# cp /fast/projects/Schulte_NB/work/Henssen_CellLine_RNA/171107_D00759_0131_ACBPD9ANXX/AS-198907-LR-30354/fastq/AS-198907-LR-30354_R1.fastq.gz /fast/users/helmsauk_c/scratch/nb-cl-rnaseq-fastq/
# mv /fast/users/helmsauk_c/scratch/nb-cl-rnaseq-fastq/AS-198907-LR-30354_R1.fastq.gz /fast/users/helmsauk_c/scratch/nb-cl-rnaseq-fastq/IMR575_DMSO1.R1.fastq.gz
# cp /fast/projects/Schulte_NB/work/Henssen_CellLine_RNA/171107_D00759_0131_ACBPD9ANXX/AS-198907-LR-30354/fastq/AS-198907-LR-30354_R2.fastq.gz /fast/users/helmsauk_c/scratch/nb-cl-rnaseq-fastq/
# mv /fast/users/helmsauk_c/scratch/nb-cl-rnaseq-fastq/AS-198907-LR-30354_R2.fastq.gz /fast/users/helmsauk_c/scratch/nb-cl-rnaseq-fastq/IMR575_DMSO1.R2.fastq.gz

WORKING_DIR = "/fast/users/helmsauk_c/work/nb-cl-rnaseq/"
FASTQ_DIR = "/fast/users/helmsauk_c/scratch/nb-cl-rnaseq-fastq/"
STAR_DIR =  "/fast/users/helmsauk_c/scratch/nb-cl-rnaseq-STAR/"
CUFFLINKS_DIR = WORKING_DIR + "cufflinks/"
KALLISTO_DIR = WORKING_DIR + "kallisto/"
MULTIQC_DIR = WORKING_DIR + "multiqc/"
FEATURECOUNTS_DIR = WORKING_DIR + "featureCounts/"
BBMAP_DIR = "/fast/users/helmsauk_c/work/miniconda/envs/qc/opt/bbmap-38.58-0/"
ADAPTER_SEQUENCES = BBMAP_DIR + "resources/adapters.fa"

# mkdir /fast/users/helmsauk_c/work/nb-cl-rnaseq/ensembl-cdna
# cd /fast/users/helmsauk_c/work/nb-cl-rnaseq/ensembl-cdna
# rsync -av rsync://ftp.ensembl.org/pub/release-97/fasta/homo_sapiens/cdna/ .
TRANSCRIPTS_FASTA = "/fast/users/helmsauk_c/work/nb-cl-rnaseq/ensembl-cdna/Homo_sapiens.GRCh38.cdna.all.fa.gz" # should be .fasta.gz
KALLISTO_INDEX = WORKING_DIR + "kallisto_index"

STAR_INDEX_DIR = "/fast/users/helmsauk_c/scratch/STARindex_GRCh37_ENSEMBL75/"
GENOME_FASTA = "/fast/projects/cubit/current/static_data/annotation/ENSEMBL/75/GRCh37/Homo_sapiens.GRCh37.75.dna.toplevel.fa"
ANNOTATION_GTF = "/fast/projects/cubit/current/static_data/annotation/ENSEMBL/75/GRCh37/Homo_sapiens.GRCh37.75.gtf"
# sed "s/^chr//" /fast/users/helmsauk_c/work/resources/rseqc-support/hg19_Ensembl_gene.bed > /fast/users/helmsauk_c/work/resources/rseqc-support/hg19_Ensembl_gene_GRCh37ChromosomeNames.bed
RSEQC_GENE_MODEL_BED = "/fast/users/helmsauk_c/work/resources/rseqc-support/hg19_Ensembl_gene_GRCh37ChromosomeNames.bed"

# EXP = [ "Henssen_IMR575_DMSO1", "Henssen_IMR575_DMSO2", "Henssen_IMR575_DMSO3",
#         "Henssen_IMR575_DMSO4", "Henssen_IMR575_DMSO5", "Henssen_IMR575_DMSO6",
#         "Harenza_CHP-212", "Harenza_Kelly", "Harenza_SK-N-DZ", "Harenza_SK-N-FI",
#         "Harenza_SK-N-SH", "Harenza_NB-69","Harenza_IMR-32",
#         "Boeva_SK-N-DZ", "Boeva_TR-14", "Boeva_SH-EP", "Boeva_CHP-212", "Boeva_hNCC-Rep1",
#         "Boeva_hNCC-Rep2", "Boeva_SK-N-SH", "Boeva_NB-69",
#         "CaltechRnaSeq_H1-hESC-Rep1", "CaltechRnaSeq_H1-hESC-Rep2", "CaltechRnaSeq_H1-hESC-Rep3" ]

metadata = pd.read_csv(WORKING_DIR + "metadata.csv", sep=";", comment="#")
# encode_metadata = pd.read_csv(WORKING_DIR + "encode_metadata.csv", sep=";", comment="#")
# encode_metadata = encode_metadata[['Sample']].drop_duplicates()
# metadata = pd.concat([metadata, encode_metadata])
EXP = metadata.SAMPLE.drop_duplicates().tolist()

rule all:
    input:
        KALLISTO_INDEX,
        expand(FASTQ_DIR + "{e}.{r}_fastqc.html", e=EXP, r=["R1","R2"]),
        expand(FASTQ_DIR + "{e}.{r}.trimmed_fastqc.html", e=EXP, r=["R1","R2"]),
        expand(KALLISTO_DIR + "{e}/abundance.tsv", e=EXP),
        expand(STAR_DIR + "{e}.Aligned.out.bam", e=EXP), # change accordingly
        expand(STAR_DIR + "{e}.Aligned.out.bam.bam_stat.txt", e=EXP),
        expand(STAR_DIR + "{e}.Aligned.out.bam.DupRate_plot.pdf", e=EXP),
        expand(STAR_DIR + "{e}.Aligned.out.bam.read_distribution.txt", e=EXP),
        expand(STAR_DIR + "{e}.Aligned.out.bam.splice_junction.pdf", e=EXP),
        expand(STAR_DIR + "{e}.Aligned.out.bam.junctionSaturation_plot.pdf", e=EXP),
        expand(STAR_DIR + "{e}.Aligned.out.bam.inner_distance_plot.pdf", e=EXP),
        expand(CUFFLINKS_DIR + "{e}/genes.fpkm_tracking", e=EXP),
        expand(FEATURECOUNTS_DIR + "{e}.Aligned.out.bam.featureCounts", e=EXP),
        MULTIQC_DIR + "multiqc_report.html"

rule fastqc:
    input:
        "{exp}.fq.gz"
    output:
        "{exp}_fastqc.html"
    conda:
        WORKING_DIR + "qc.yaml"
    shell:
        "fastqc {input}"

rule adapter_trimming:
    input:
        r1 = FASTQ_DIR + "{exp}.R1.fq.gz",
        r2 = FASTQ_DIR + "{exp}.R2.fq.gz"
    output:
        r1_trimmed = FASTQ_DIR + "{exp}.R1.trimmed.fq.gz",
        r2_trimmed = FASTQ_DIR + "{exp}.R2.trimmed.fq.gz"
    conda:
        WORKING_DIR + "qc.yaml"
    shell:
        BBMAP_DIR + "bbduk.sh in1={input.r1} in2={input.r2} out1={output.r1_trimmed} out2={output.r2_trimmed} ktrim=r k=23 mink=11 hdist=1 ref=" + ADAPTER_SEQUENCES + " tbo"

rule star_index:
    input:
        genome = GENOME_FASTA,
        annotation = ANNOTATION_GTF
    output:
        starindex = STAR_INDEX_DIR + "SA"
    params:
        outdir = STAR_INDEX_DIR
    conda:
        WORKING_DIR + "star.yaml"
    shell:
        "mkdir -p {params.outdir} && STAR --runThreadN 10 --runMode genomeGenerate --genomeDir {params.outdir} --genomeFastaFiles {input.genome} --sjdbGTFfile {input.annotation} --sjdbOverhang 100 --limitGenomeGenerateRAM 95000000000"

rule star:
    input:
        starindex = STAR_INDEX_DIR + "SA",
        fastq_r1 = FASTQ_DIR + "{exp}.R1.trimmed.fq.gz",
        fastq_r2 = FASTQ_DIR + "{exp}.R2.trimmed.fq.gz"
    output:
        star_unsorted = STAR_DIR + "{exp}.Aligned.out.bam",
        star_sorted = STAR_DIR + "{exp}.Aligned.sortedByCoord.out.bam"
    params:
        e = lambda wildcards: {wildcards.exp},
        starindex_dir = STAR_INDEX_DIR
    conda:
        WORKING_DIR + "star.yaml"
    shell:
        "STAR --genomeDir {params.starindex_dir} --runThreadN 12 --readFilesIn {input.fastq_r1} {input.fastq_r2} --outFileNamePrefix " + STAR_DIR + "{params.e}. --outSAMtype BAM Unsorted SortedByCoordinate --outSAMunmapped Within --outSAMattributes Standard --readFilesCommand zcat"

rule rseqc_bam_stat:
    input:
        STAR_DIR + "{exp}.Aligned.out.bam"
    output:
        STAR_DIR + "{exp}.Aligned.out.bam.bam_stat.txt"
    conda:
        WORKING_DIR + "qc.yaml"
    shell:
        "bam_stat.py -i {input} > {output}"

rule rseqc_read_duplication:
    input:
        STAR_DIR + "{exp}.Aligned.out.bam"
    output:
        STAR_DIR + "{exp}.Aligned.out.bam.DupRate_plot.pdf"
    params:
        exp = STAR_DIR + "{exp}"
    conda:
        WORKING_DIR + "qc.yaml"
    shell:
        "read_duplication.py -i {input} -o {input}"

rule rseqc_junction_annotation:
    input:
        STAR_DIR + "{exp}.Aligned.out.bam"
    output:
        STAR_DIR + "{exp}.Aligned.out.bam.splice_junction.pdf"
    params:
        exp = STAR_DIR + "{exp}",
        gene_model_bed = RSEQC_GENE_MODEL_BED
    conda:
        WORKING_DIR + "qc.yaml"
    shell:
        "junction_annotation.py -i {input} -o {input} -r {params.gene_model_bed}"

rule rseqc_junction_saturation:
    input:
        STAR_DIR + "{exp}.Aligned.out.bam"
    output:
        STAR_DIR + "{exp}.Aligned.out.bam.junctionSaturation_plot.pdf"
    params:
        exp = STAR_DIR + "{exp}",
        gene_model_bed = RSEQC_GENE_MODEL_BED
    conda:
        WORKING_DIR + "qc.yaml"
    shell:
        "junction_saturation.py -i {input} -o {input} -r {params.gene_model_bed}"

rule rseqc_inner_distance:
    input:
        STAR_DIR + "{exp}.Aligned.out.bam"
    output:
        STAR_DIR + "{exp}.Aligned.out.bam.inner_distance_plot.pdf"
    params:
        exp = STAR_DIR + "{exp}",
        gene_model_bed = RSEQC_GENE_MODEL_BED
    conda:
        WORKING_DIR + "qc.yaml"
    shell:
        "inner_distance.py -i {input} -o {input} -r {params.gene_model_bed}"

rule rseqc_read_distribution:
    input:
        STAR_DIR + "{exp}.Aligned.out.bam"
    output:
        STAR_DIR + "{exp}.Aligned.out.bam.read_distribution.txt"
    params:
        exp = STAR_DIR + "{exp}",
        gene_model_bed = RSEQC_GENE_MODEL_BED
    conda:
        WORKING_DIR + "qc.yaml"
    shell:
        "read_distribution.py -i {input} -r {params.gene_model_bed} > {output}"

rule featurecounts_genes:
    input:
        bam = STAR_DIR + "{exp}.Aligned.out.bam",
        annotation = ANNOTATION_GTF
    output:
        FEATURECOUNTS_DIR + "{exp}.Aligned.out.bam.featureCounts"
    conda:
        WORKING_DIR + "featureCounts.yaml"
    shell:
        "featureCounts -p -t exon -g gene_id -a {input.annotation} -o {output} {input.bam}"

rule cufflinks:
    input:
        bam = STAR_DIR + "{exp}.Aligned.sortedByCoord.out.bam",
        annotation = ANNOTATION_GTF
    output:
        CUFFLINKS_DIR + "{exp}/genes.fpkm_tracking"
    params:
        exp = lambda wildcards : {wildcards.exp}
    conda:
        WORKING_DIR + "cufflinks.yaml"
    shell:
        "cufflinks {input.bam} --GTF {input.annotation} --output-dir " + CUFFLINKS_DIR + "{params.exp} --num-threads 12"

rule kallisto_index:
    input:
        transcripts = TRANSCRIPTS_FASTA
    output:
        index = KALLISTO_INDEX
    conda:
        WORKING_DIR + "kallisto.yaml"
    shell:
        "kallisto index -i {output.index} {input.transcripts}"

rule kallisto:
    input:
        kallisto_index = KALLISTO_INDEX,
        fastq_r1 = FASTQ_DIR + "{exp}.R1.trimmed.fq.gz",
        fastq_r2 = FASTQ_DIR + "{exp}.R2.trimmed.fq.gz"
    output:
        KALLISTO_DIR + "{exp}/abundance.tsv"
    params:
        e = lambda wildcards: {wildcards.exp}
    conda:
        WORKING_DIR + "kallisto.yaml"
    shell:
        "kallisto quant -i {input.kallisto_index} -o " + KALLISTO_DIR + "{params.e} -b 100 {input.fastq_r1} {input.fastq_r2}"

# Amerigo Gazeway: B.I.G. Poppa's Got A Brand New Bag [Clean]
# Album: The Notorious J.B.'s - B.I.G. Poppa's Got A Brand New Bag
# Soul Mates Recrds


# Harenza IMR-32 misses from featureCounts
# multiqc /fast/users/helmsauk_c/work/nb-cl-rnaseq/featureCounts /fast/users/helmsauk_c/work/nb-cl-rnaseq/cufflinks /fast/users/helmsauk_c/scratch/nb-cl-rnaseq-fastq/ /fast/users/helmsauk_c/scratch/nb-cl-rnaseq-STAR/

rule multiqc:
    input:
        expand(FASTQ_DIR + "{e}.{r}_fastqc.html", e=EXP, r=["R1","R2"]),
        expand(FASTQ_DIR + "{e}.{r}.trimmed_fastqc.html", e=EXP, r=["R1","R2"]),
        expand(STAR_DIR + "{e}.Aligned.out.bam.bam_stat.txt", e=EXP),
        expand(STAR_DIR + "{e}.Aligned.out.bam.DupRate_plot.pdf", e=EXP),
        expand(STAR_DIR + "{e}.Aligned.out.bam.read_distribution.txt", e=EXP),
        expand(STAR_DIR + "{e}.Aligned.out.bam.splice_junction.pdf", e=EXP),
        expand(STAR_DIR + "{e}.Aligned.out.bam.junctionSaturation_plot.pdf", e=EXP),
        expand(STAR_DIR + "{e}.Aligned.out.bam.inner_distance_plot.pdf", e=EXP),
        expand(CUFFLINKS_DIR + "{e}/genes.fpkm_tracking", e=EXP),
        expand(FEATURECOUNTS_DIR + "{e}.Aligned.out.bam.featureCounts", e=EXP)
    output:
        MULTIQC_DIR + "multiqc_report.html"
    conda:
        WORKING_DIR + "qc.yaml"
    shell:
        "multiqc " + FASTQ_DIR + " " + STAR_DIR + " " + CUFFLINKS_DIR + " " + FEATURECOUNTS_DIR + " -o " + MULTIQC_DIR
